Using CNNs to recognize hand gestures
====

This is the repository for the conclusion work of André Guedes a student at University of Brasília - UnB

Abstract
--
With the technological advancement of parallel computing platforms, the opportunity to explore Convolutional Neural Networks (CNN) has emerged. The popularization of GPU computing enables the solution of real-life problems in a smaller time-frame than ever before. The objective of this work is to make use of CNNs in the scope of hand shape classification. Some possibilities are investigated, such as real time recognition, refining of pre-trained CNNs, training networks from scratch and hand tracking. An introduction to the relevant concepts of convolutional neural networks as well as a revision of the state-of-the-art were performed to constitute a base theoretical framework in which this work is grounded. From this literature review, we decided to build upon the work of Koller et al. Also, experiments to reproduce Koller et al.'s results are presented. This reports also presents a plan of future efforts oriented by the obtained results.


Work
--

The first iteration of the work was focused on the investigation of the use of Convolutional Networks for handshape recognition.

This investigation was carried out by reproducing the results of the paper [Deep Hand: How to Train a CNN on 1 Million Hand Images When Your Data Is Continuous and Weakly Labelled](http://www-i6.informatik.rwth-aachen.de/~koller/1miohands/) by Koller et al.

These results were the state-of-the-art when the first iteration of the research took place.

Using the model provided by the work authors, a realtime handshape classifier was built.

For the scripts and experiments see the [`src`](https://gitlab.com/andrebsguedes/gesture-cnn/tree/master/src) folder.
