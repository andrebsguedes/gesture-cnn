#!/bin/env python3
import os
from subprocess import Popen

lines = []
with open("data/val.txt") as f:
    lines = f.readlines()

known = {"1" : 1, "2": 2, "3": 3, "3_hook": 4, "4": 5, "5": 6, "a": 10, "ae": 55, "b": 11, "b_nothumb": 12, "b_thumb": 13, "by": 16, "c": 17, "cbaby": 14, "d": 18, "e": 19, "f": 20, "f_open": 21, "fly": 22, "fly_nothumb": 23, "g": 24, "h": 25, "h_hook": 26, "h_thumb": 27, "i": 28, "index": 36, "index_flex": 37, "index_hook": 38, "ital": 40, "ital_thumb": 41, "ital_nothumb": 42, "ital_open": 43, "jesus": 29, "k": 30, "l_hook": 31, "o": 35, "obaby": 15, "pincet": 39, "r": 44, "s": 45, "v": 49, "v_flex": 50, "w": 53, "write": 46, "y": 54}

with open("data/valtest.txt", "w") as p:
    for line in lines:
        parts = line.split(" ")
        label = parts[1]
        label = label.rstrip('\n')
        parts[1] = " {}\n".format(known[label])
        converted_line = parts[0] + parts[1]
        p.write(converted_line)
