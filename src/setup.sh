virtualenv --no-site-packages --distribute .env && \
  source .env/bin/activate && \
  pip install numpy && \
  pip install -r requirements.txt && \
  ln -s `/usr/bin/python -c 'import cv2; print(cv2.__file__)'` .env/lib/`ls -1 .env/lib`/site-packages/
# cd protobuf/python && \
# python setup.py build --cpp_implementation && \
# python setup.py install --cpp_implementation && \
# rm -rf dist && \
# cd ../.. && \
# echo export PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION=cpp >> .env/bin/activate &&\
