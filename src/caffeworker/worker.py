import caffe, lmdb, os
import numpy as np
import cv2, queue, time
from caffeworker import downloader as dl
from caffe.proto import caffe_pb2
from subprocess import Popen, PIPE

caffe_root = os.getenv("CAFFE_ROOT")
home_folder = os.getenv("HOME")

caffe.set_mode_gpu()

def load_model():
    model_def, model_weights = dl.download_model()
    model_def = os.path.join(os.path.dirname(__file__), 'models', 'custom', 'submit-net-manual.prototxt')
    return model_def, model_weights

def load_mean():
    mu = np.load(os.path.join(os.path.dirname(__file__), 'models', 'custom', 'mean.npy'))
    return mu.mean(1).mean(1)

def load_transformer(shape, mean):
    transformer = caffe.io.Transformer({'data': shape})
    transformer.set_transpose('data', (2,0,1))  # move image channels to outermost dimension
    transformer.set_mean('data', mean)            # subtract the dataset-mean value in each channel
    #transformer.set_channel_swap('data', (2,1,0))  # swap channels from RGB to BGR
    return transformer

class NetHandler:

    __shared_state = {}

    def __init__(self):
        if NetHandler.__shared_state:
            self.__dict__ = NetHandler.__shared_state
        else:
            mean = load_mean()

            model_def, model_weights = load_model()
            self._net = caffe.Net(model_def, caffe.TEST, weights=model_weights)

            self._transformer = load_transformer(self._net.blobs['data'].shape, mean)

            NetHandler.__shared_state = self.__dict__


    def get_net_and_transformer(self):
        return self._net, self._transformer


    def forward_image(self, image_path):
        image = image_vector_pb2.Image()

        net, transformer = self.get_net_and_transformer()

        shape = net.blobs['data'].data.shape

        #labels = load_labels()

        net.blobs['data'].reshape(1, shape[1], shape[2], shape[3])

        image_data = cv2.imread(image_path)

        net.blobs['data'].data[...] = transformer.preprocess(net.inputs[0], image_data)

        out = net.forward()

        probs = out['loss3_r'][0]

        image.path = image_path
        image.probability.extend(probs.tolist())
        image.label = str(probs.argmax())

        return image

    def forward_frames(self, capture_device, limages):
        #image = image_vector_pb2.Image()

        net, transformer = self.get_net_and_transformer()

        shape = net.blobs['data'].data.shape

        #labels = load_labels()

        net.blobs['data'].reshape(1, shape[1], shape[2], shape[3])

        cap = cv2.VideoCapture(capture_device)

        while(True):

            ret, frame = cap.read()

            x, y, c = frame.shape

            crop = frame[int(y/2-175):int(y/2+175), int(x/2-100):int(x/2+100)]

            #image_data = crop
            image_data = cv2.resize(crop, (227, 227))

            net.blobs['data'].data[...] = transformer.preprocess(net.inputs[0], image_data)

            out = net.forward()

            probs = out['loss3_r'][0]

            if probs.argmax() != 0:
                label = limages[probs.argmax() - 1]
                crop[-76:-1,-76:-1,:] = label
                print(probs.argmax())

            cv2.imshow('frame', crop)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        cap.release()
        cv2.destroyAllWindows()

    def get_vector_from_lmdb(self, image_list_file, lmdb_path, batch_size):
        #image_vector = image_vector_pb2.ImageVector()
        image_paths = load_image_paths(image_list_file)

        env = lmdb.open(lmdb_path)
        transaction = env.begin()
        length = transaction.stat()['entries']
        cursor = transaction.cursor()
        datum = caffe_pb2.Datum()

        net, transformer = self.get_net_and_transformer()

        shape = net.blobs['data'].data.shape

        print(env.stat())
        #labels = load_labels()

        extra = 0
        if length % batch_size != 0 :
            extra = 1
        buckets = length // batch_size + extra

        c = 0
        cursor.first()
        for n in range(buckets):
            print("Batch: {}".format(n))
            labels = []
            input_size = batch_size
            if n == buckets - 1:
                input_size = length % batch_size

            net.blobs['data'].reshape(input_size, shape[1], shape[2], shape[3])

            for j in range(input_size):
                datum.ParseFromString(cursor.value())
                data = caffe.io.datum_to_array(datum)
                data = data.astype(np.uint8)
                data = np.transpose(data, (1,2,0))
                labels.append(datum.label)

                net.blobs['data'].data[j] = transformer.preprocess(net.inputs[0], data)
                cursor.next()

            out = net.forward()

            probs = out['loss3_r']

            for j in range(len(probs)):
                #image = image_vector.image.add()
                #image.path = image_paths[c]
                #image.probability.extend(probs[j].tolist())
                #image.label = str(labels[j])
                c+=1

        return None# image_vector

def get_labels(folder):
    limages = []
    for i in range(1, 61):
        res = cv2.resize(cv2.imread(folder + '/' + str(i) + '.png', cv2.IMREAD_COLOR), (75, 75))
        limages.append(res)

    return limages

#def init_worker(n):
#    buf = "k_nearest/data.protobuf"
#    if os.path.isfile(buf):
#        t = time.time()
#        vector = load_buffer("k_nearest/data.protobuf")
#        print("Load Buffer: {}".format(time.time() - t))
#    else:
#        vector = n.get_vector_from_lmdb("k_nearest/data/val.txt","k_nearest/db", 16)
#        write_buffer(vector, buf)
#
#    t = time.time()
#    fast_array = to_fast_array(vector)
#    print("Fast Load: {}".format(time.time() - t))
#
#    return vector, fast_array

n = NetHandler()
#vector, fast_array = init_worker(n);
#image_vector = n.get_vector_from_lmdb("/home/andrebsguedes/software/tcc/eval/1miohands-modelzoo-v2/data/valtest.txt", "/home/andrebsguedes/software/tcc/eval/1miohands-modelzoo-v2/val_lmdb_view", 32)
#write_buffer(image_vector, "data.protobuf")


#vector = to_fast_array(load_buffer("data.protobuf"))
#
#counts = np.zeros([61, 61], dtype=np.int32)
#confusion = np.zeros([13, 13])
#
#mapping = { 1: 0, 2: 1, 3: 2, 6: 3, 11: 4, 13: 5, 17: 6, 20: 7, 24: 8, 36: 9, 40: 10, 35: 11, 45: 12}
#
#np.set_printoptions(threshold=np.nan, precision=2)
#
#for image in vector:
#    i = np.argmax(image['vector'])
#    j = int(image['label'])
#    counts[i][j] += 1
#
#sums = np.sum(counts, axis = 1)
#for i in range(61):
#    for j in range(61):
#        if j in mapping and i in mapping:
#            confusion[mapping[i]][mapping[j]] = (counts[i][j] / sums[j]) * 100.0
#
#
#with open('testit.txt', 'w') as f:
#    for i in range(13):
#        for j in range(13):
#            if j != 12:
#                f.write("{:.1f}\t".format(confusion[i][j]))
#            else:
#                f.write("{:.1f}\n".format(confusion[i][j]))

#print(n.forward_image('k_nearest/data/me.jpg'))
def run(camera_device=0):
    n.forward_frames(camera_device, get_labels(os.path.join(os.path.dirname(__file__), 'models', 'custom', 'handicons')))

#print(confusion)

#get_vector_from_lmdb("data/valsingle.txt","db", 64)
#write_buffer(image_vector, "test.protobuf")
