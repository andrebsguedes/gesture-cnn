import urllib.request, time, sys, os, tarfile

def reporthook(count, block_size, total_size):
    """
    From http://blog.moleculea.com/2012/10/04/urlretrieve-progres-indicator/
    """
    global start_time
    if count == 0:
        start_time = time.time()
        return
    duration = (time.time() - start_time) or 0.01
    progress_size = int(count * block_size)
    speed = int(progress_size / (1024 * duration))
    percent = int(count * block_size * 100 / total_size)
    sys.stdout.write("\r...%d%%, %d MB, %d KB/s, %d seconds passed" %
                    (percent, progress_size / (1024 * 1024), speed, duration))
    sys.stdout.flush()

def download_url(url, filename, hook):
    urllib.request.urlretrieve(url, filename, hook)

def download_model():
    tarpath = os.path.join(os.path.dirname(__file__), 'models', '1miohands-modelzoo-v2.tar.gz')

    if not os.path.isfile(tarpath):
        download_url('ftp://wasserstoff.informatik.rwth-aachen.de/pub/rwth-phoenix/2016/1miohands-modelzoo-v2.tar.gz', tarpath, reporthook)

    tarobj = tarfile.open(tarpath)
    tarobj.extractall(os.path.join(os.path.dirname(__file__), 'models'))
    folder = os.path.join(os.path.dirname(__file__), 'models', '1miohands-modelzoo-v2')
    return os.path.join(folder, 'submit-net.prototxt'), os.path.join(folder, '1miohands-v2.caffemodel')
