# Experiments

Source code for empirical experiments

### Installation

Dependencies:

- Python 3
- pip
- virtualenv
- OpenCV 3 with python bindings
- Caffe with pycaffe

After acquiring all dependencies to install run `./setup.sh` 

When installation is completed run `source .env/bin/activate` to enter the created virtualenv.



#### Realtime Handshape Classifier

To use the classifier run `python camera_classifier.py`

OBS.: Make sure your Webcam is the first video device or change `camera_classifier.py` to use your device number.



#### Mapping Converter

Simple script to convert the label mapping on the provided evaluation dataset.

To use the script run `python convert_mapping.py`